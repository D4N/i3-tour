use gtk::prelude::*;

pub fn add_img_box(container: &gtk::Box, resource_uri: &str, head: String, body: String) {
    let image = gtk::Image::from_resource(&resource_uri);
    image.set_valign(gtk::Align::Start);
    image.show();

    let head_label = gtk::LabelBuilder::new()
        .label(&head)
        .justify(gtk::Justification::Center)
        .valign(gtk::Align::Center)
        .margin_top(36)
        .build();
    head_label.get_style_context().add_class("page-title");
    head_label.show();

    let body_label = gtk::LabelBuilder::new()
        .label(&body)
        .lines(2)
        .wrap(true)
        .justify(gtk::Justification::Center)
        .valign(gtk::Align::Center)
        .margin_top(12)
        .build();
    body_label.get_style_context().add_class("page-body");
    body_label.show();

    container.add(&head_label);
    container.add(&image);
    container.add(&body_label);
}
