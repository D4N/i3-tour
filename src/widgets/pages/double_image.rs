use gtk::prelude::*;

use super::image_common::add_img_box;

pub struct DoubleImagePageWidget {
    pub widget: gtk::Box,
}

impl DoubleImagePageWidget {
    pub fn new(resource_uris: [&str; 2], heads: [&String; 2], bodies: [&String; 2]) -> Self {
        let widget = gtk::Box::new(gtk::Orientation::Vertical, 0);

        let image_page = Self { widget };

        let container = image_page.init_container();

        add_img_box(
            &container,
            resource_uris[0],
            heads[0].to_string(),
            bodies[0].to_string(),
        );
        add_img_box(
            &container,
            resource_uris[1],
            heads[1].to_string(),
            bodies[1].to_string(),
        );

        container.show();
        image_page.widget.add(&container);
        image_page.widget.show();

        image_page
    }

    fn init_container(&self) -> gtk::Box {
        self.widget.set_property_expand(true);
        self.widget.get_style_context().add_class("page");
        self.widget.set_halign(gtk::Align::Fill);
        self.widget.set_valign(gtk::Align::Fill);

        let container = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .spacing(12)
            .halign(gtk::Align::Center)
            .valign(gtk::Align::Center)
            .vexpand(true)
            .margin_bottom(48)
            .margin_top(12)
            .margin_start(12)
            .margin_end(12)
            .build();

        container
    }
}
