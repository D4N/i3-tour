use crate::utils::i18n_f;
use crate::widgets::pages::ButtonsWidget;
use crate::widgets::pages::DoubleImagePageWidget;
use gettextrs::gettext;
use gtk::prelude::*;
use std::cell::RefCell;
use std::process::Command;
use std::rc::Rc;

use lazy_static::lazy_static;
use regex::Regex;

use super::pages::{ImagePageWidget, WelcomePageWidget};
use super::paginator::PaginatorWidget;
use crate::config::{APP_ID, PROFILE};

pub struct Window {
    pub widget: libhandy::ApplicationWindow,
    pub paginator: RefCell<Rc<PaginatorWidget>>,
}

fn get_modifier() -> Option<String> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^\s*set\s+\$mod\s+(?P<mod>.*)").unwrap();
    }

    let config_output = Command::new("i3-msg")
        .args(&["-t", "get_config"])
        .output()
        .expect("Failed to execute i3-msg");

    for line in String::from_utf8_lossy(&config_output.stdout).split("\n") {
        // let line = str::from_utf8(&line[..]);
        match RE.captures(&line) {
            Some(m) => {
                return match &m["mod"] {
                    "Mod1" => Some("Alt".to_string()),
                    "Mod4" => Some("Super".to_string()),
                    m => Some(m.to_string()),
                }
            }
            None => continue,
        };
    }
    None
}

impl Window {
    pub fn new(app: &gtk::Application) -> Self {
        let widget = libhandy::ApplicationWindow::new();
        widget.set_application(Some(app));

        let paginator = RefCell::new(PaginatorWidget::new());

        let mut window_widget = Window { widget, paginator };

        window_widget.init();
        window_widget
    }

    pub fn start_tour(&self) {
        self.paginator.borrow_mut().set_page(1);
    }

    pub fn reset_tour(&self) {
        self.paginator.borrow_mut().set_page(0);
    }

    fn init(&mut self) {
        self.widget.set_default_size(960, 720);
        self.widget.set_icon_name(Some(APP_ID));

        let modifier = get_modifier().or(Some("Alt".to_string())).unwrap();

        // Devel Profile
        if PROFILE == "Devel" {
            self.widget.get_style_context().add_class("devel");
        }
        self.paginator.borrow_mut().add_page(
            WelcomePageWidget::new(&modifier)
                .widget
                .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/terminal.png",
                gettext("Open a terminal"),
                i18n_f("Press {}+Enter to open a terminal.", &[&modifier]),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/terminals.png",
                gettext("Open another terminal"),
                i18n_f(
                    "Press {}+Enter again to open another terminal window.",
                    &[&modifier],
                ),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/terminals_horizontal.png",
                gettext("Switch to a horizontal split"),
                i18n_f(
                    "Press {}+e to toggle between the vertical and horizontal split.",
                    &[&modifier],
                ),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/terminals_tabbed.png",
                gettext("Switch to the tabbed layout"),
                i18n_f("Press {}+w to switch to the tabbed layout.", &[&modifier]),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/terminals_stacked.png",
                gettext("Switch to the stacked layout"),
                i18n_f("Press {}+s to switch to the stacked layout.", &[&modifier]),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/desktop.png",
                gettext("Close the focused window"),
                i18n_f(
                    "Press {}+q to close the window that is currently in focus.",
                    &[&modifier],
                ),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/dmenu.png",
                gettext("Launch applications"),
                i18n_f("Press {}+d to start dmenu. Enter the name of the application to launch and press Enter to launch it.", &[&modifier]),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/workspace.png",
                gettext("Switch workspaces"),
                i18n_f("Press {}+n to switch to the n-th workspace. To move the currently active window to the n-th workspace use {}+Shift+n.", &[&modifier, &modifier]),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/move-around.png",
                gettext("Switching windows"),
                i18n_f("You can change the focus to a different window via {}+j/k/l/; or {}+←,↓,↑,→, to move the focus left, down, up or right. Or hover over the window with the mouse.", &[&modifier, &modifier]),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ImagePageWidget::new(
                "/org/i3/Tour/exit.png",
                gettext("Exiting i3"),
                i18n_f("Quit your i3 session and return back to the login screen by pressing {}+Shift+e.", &[&modifier]),
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            DoubleImagePageWidget::new(
                [
                    "/org/i3/Tour/keyboard-layer1.png",
                    "/org/i3/Tour/keyboard-layer2.png",
                ],
                [
                    &i18n_f("Keys to use with $mod ({})", &[&modifier]),
                    &i18n_f("Keys to use with Shift+$mod ({})", &[&modifier]),
                ],
                [
                    &i18n_f(
                        "These are all the shortcuts available when pressing {}.",
                        &[&modifier],
                    ),
                    &i18n_f(
                        "And the following shortcuts become available when pressing {}+Shift.",
                        &[&modifier],
                    ),
                ],
            )
            .widget
            .upcast::<gtk::Widget>(),
        );

        self.paginator.borrow_mut().add_page(
            ButtonsWidget::new(&modifier, which::which("liveinst").is_ok())
                .widget
                .upcast::<gtk::Widget>(),
        );

        self.widget.add(&self.paginator.borrow().widget);
    }
}
