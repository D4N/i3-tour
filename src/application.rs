use crate::config;
use crate::utils;
use crate::widgets::Window;
use gio::prelude::*;
use glib::clone;
use gtk::prelude::*;
use log::info;
use std::env;
use std::io::Write;
use std::path::PathBuf;
use std::process::Command;
use std::{cell::RefCell, rc::Rc};
use xdg;

pub struct Application {
    app: gtk::Application,
    window: RefCell<Rc<Option<Window>>>,
}

impl Application {
    pub fn new() -> Rc<Self> {
        let app =
            gtk::Application::new(Some(config::APP_ID), gio::ApplicationFlags::FLAGS_NONE).unwrap();

        let application = Rc::new(Self {
            app,
            window: RefCell::new(Rc::new(None)),
        });

        application.setup_signals(application.clone());
        application
    }

    fn setup_gactions(&self, application: Rc<Self>) {
        // Quit
        utils::action(
            &self.app,
            "quit",
            clone!(@strong self.app as app => move |_, _| {
                app.quit();
            }),
        );

        // Start Tour
        utils::action(
            &self.app,
            "start-tour",
            clone!(@strong application => move |_, _| {
                if let Some(window) = &*application.window.borrow().clone() {
                    window.start_tour();
                }
            }),
        );

        // Skip Tour
        utils::action(
            &self.app,
            "skip-tour",
            clone!(@strong self.app as app => move |_, _| {
                app.quit();
            }),
        );

        utils::action(
            &self.app,
            "next-page",
            clone!(@strong application => move |_, _| {
                if let Some(window) = &*application.window.borrow().clone() {
                    if window.paginator.borrow_mut().try_next().is_none() {
                        window.widget.close();
                    }
                }
            }),
        );

        utils::action(
            &self.app,
            "previous-page",
            clone!(@strong application => move |_, _| {
                if let Some(window) = &*application.window.borrow().clone() {
                    if window.paginator.borrow_mut().try_previous().is_none() {
                        window.reset_tour();
                    }
                }
            }),
        );

        if which::which("liveinst").is_ok() {
            utils::action(&self.app, "launch-liveinst", |_, _| {
                Command::new("liveinst")
                    .spawn()
                    .expect("liveinst failed to launch");
            });
        }

        utils::action(&self.app, "open-config", |_, _| {
            xdg::BaseDirectories::with_prefix("i3").map(|i3_dir| {
                let editor = env::var("EDITOR").unwrap_or("mousepad".to_string());
                let editor: Vec<&str> = editor.split(' ').collect();
                let conf_file = i3_dir.find_config_file("config");
                let path = match conf_file {
                    None => {
                        let config_output = Command::new("i3-msg")
                            .args(&["-t", "get_config"])
                            .output()
                            .expect("Failed to execute i3-msg");
                        let config_path = i3_dir
                            .place_config_file("config")
                            .unwrap_or(PathBuf::from("~/.config/i3/config"));

                        std::fs::File::create(&config_path)
                            .map(|mut f| f.write_all(&config_output.stdout))
                            .map_err(|e| {
                                println!(
                                    "writing the i3 config to {:?} failed with {:?}",
                                    config_path, e
                                );
                            });

                        config_path
                    }
                    Some(path) => path,
                };

                let mut cmd = Command::new(editor[0]);
                for i in 1..editor.len() {
                    cmd.arg(editor[i]);
                }
                cmd.arg(path);

                cmd.spawn().map_err(|e| {
                    println!("failed to launch editor {}, got {:?}", editor[0], e);
                    e
                });
            });
        });

        utils::action(&self.app, "open-i3-userguide", |_, _| {
            Command::new("xdg-open")
                .arg("https://i3wm.org/docs/userguide.html")
                .spawn()
                .map_err(|e| {
                    println!("failed to open i3 usersguide via xdg-open, got {:?}", e);
                });
        });

        self.app.set_accels_for_action("app.quit", &["<primary>q"]);
    }

    fn setup_signals(&self, app: Rc<Self>) {
        self.app.connect_startup(clone!(@weak app => move |_| {
            libhandy::init();
            app.setup_css();
            app.setup_gactions(app.clone());
        }));
        self.app
            .connect_activate(clone!(@weak app => move |gtk_app| {
               let window = Window::new(&gtk_app);
                gtk_app.add_window(&window.widget);
                window.widget.present();
                window.widget.show();
                app.window.replace(Rc::new(Some(window)));
            }));
    }

    fn setup_css(&self) {
        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/org/i3/Tour/style.css");
        if let Some(screen) = gdk::Screen::get_default() {
            gtk::StyleContext::add_provider_for_screen(&screen, &p, 500);
        }
    }

    pub fn run(&self) {
        info!("i3 wm Tour ({})", config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }
}
